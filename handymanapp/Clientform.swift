//
//  Clientform.swift
//  lawyercasediary
//
//  Created by MacStudent on 2018-03-22.
//  Copyright © 2018 kamla sharma. All rights reserved.
//

import Foundation
class Clientform{
    
    public private(set) var name :String
   
    public private(set) var address :String!
    public private(set) var email :String!
    public private(set) var occupation:String
    public private(set) var mobile :Int
    public private(set) var city :String
    public private(set) var cases : String
    
    init(name: String, address :String!, email :String!, occupation :String, mobile :Int, city :String,cases :String )
    {
        self.name = name
       
        self.address = address
        self.email = email
        self.occupation = occupation
        self.mobile = mobile
        self.city = city
        self.cases = cases
        }
    public func setName(name :String!){
        if (name.isEmpty){
            self.name = " "
        } else {
            self.name = name
        }
    }
    
    public func setAddress(address: String!){
        self.address = address
    }
    public func setemail(email: String!){
        self.email = email
    }
    
    public func setoccupation(occupation: String!){
        self.occupation = occupation
    }
    
    public func setmobile ( mobile : Int!)
    {
        self.mobile  =  mobile
    }
    public func setcity(city: String!)
    {
        self.city = city
    }
    
    public func setcases(cases: String!)
    {
        self.cases = cases
        
    }
}
