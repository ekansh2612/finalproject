//
//  Client.swift
//  lawyercasediary
//
//  Created by MacStudent on 2018-03-20.
//  Copyright © 2018 kamla sharma. All rights reserved.
//

import UIKit

class Client
{
    public private(set) var name :String
    public private(set) var email :String
    public private(set) var address :String
    public private(set) var occupation :String
    public private(set) var mobile :String
    public private(set) var cases :String
   // public private(set) var sub :clientViewController!
    
    
    init(name: String, email: String, address: String,occupation:String,mobile: String,cases: String)//,sub: clientViewController)
    {
        self.name = name
        self.email = email
        self.address = address
        self.occupation = occupation
        self.mobile = mobile
        self.cases = cases
        //self.sub = sub
    }
    public func setName(name :String!)
    {
        if (name.isEmpty){
            self.name = " "
        } else {
            self.name = name
        }
    }
    public func setemail(email :String!)
    {
        if (email.isEmpty){
            self.email = " "
        } else {
            self.email = email
        }
    }
    
    public func setaddress(address :String!)
    {
        if (address.isEmpty){
            self.address = " "
        } else {
            self.address = address
        }
    }
    public func setoccupation(occupation :String!){
        if (occupation.isEmpty){
            self.occupation = " "
        } else {
            self.occupation = occupation
        }
    }
    public func setmob(mobile :String!){
    //if (mobile.isEmpty){
     //   self.mobile = 0
       // } else {
        self.mobile = mobile
        }
    
    public func setcases(cases :String!){
        if (cases.isEmpty){
            self.cases = " "
        } else {
            self.cases = cases
        }
    }
   // public func setsub(sub :clientViewController!){
       // self.sub = sub
   // }
}

