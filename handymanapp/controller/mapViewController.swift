//
//  mapViewController.swift
//  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit
import MapKit

final class mapAnnotation: NSObject ,MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?)
    {
        self.coordinate = coordinate
       self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
}


class mapViewController: UIViewController {
    
    @IBOutlet weak var lblcity: UILabel!
    
    
    @IBOutlet weak var lblname: UILabel!
    
    //@IBOutlet weak var map: MKMapView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    public var i : Int =  0
   // public var x : Int = 0

    var namex = ""
    var cityx = ""
    public var x : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("........name......")
print(namex)
print(cityx)
        
        lblname.text = namex
        lblcity.text = cityx
        
        
    
    
       
         // var x : Int = 0
        for all in nameallcities
        {
            if(all == cityx)
            {
                x = i
            }
            i = i + 1
        }
        
            print(x)        // Do any additional setup after loading the vi
        
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        
        let longitudec : Double = Double(long[x])!
        let latitudec : Double = Double(lat[x])!
        
        let citycoordinates = CLLocationCoordinate2D(latitude: latitudec, longitude: longitudec)
        
        let cityannotation = mapAnnotation(coordinate: citycoordinates, title: cityx, subtitle: namex)
        
        mapView.addAnnotation(cityannotation)
    
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
/*map.register(MKMarkerAnnotationView.self,forAnnotationViewWithReuseIdentifier:MKMapViewDefaultAnnotationViewReuseIdentifier)
   // let latx = lat[x]
   // let lonx = long[x]
    
    let coordinate = CLLocationCoordinate2D(latitude: latx!, longitude:  lonx!)
    
    let aAnnotation  = Annotation(coordinate: coordinate)
    
map.addAnnotation(aAnnotation)
 */
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension mapViewController  : MKMapViewDelegate
{
    func mapView(_ mapView :MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if let ann = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier) as? MKMarkerAnnotationView
        {
            ann.animatesWhenAdded = true
            ann.titleVisibility = .adaptive
            ann.titleVisibility = .adaptive
            
            return ann
            
        }
        return nil
        
        
    }
}

