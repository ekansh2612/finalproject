//
//  quickViewController.swift
//  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit

class quickViewController: UIViewController {

  var getname = String()
    
    
    @IBOutlet weak var lblnc: UILabel!
    
    
    @IBOutlet weak var txtnote: UITextField!
    
    
    @IBOutlet weak var btnsv: UIButton!
    
    @IBOutlet weak var btnothr: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblnc.text! = getname

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func savebtn(_ sender: UIButton) {
        
        var myAlert = UIAlertController(title:"Alert",message:"Notes saved successfully",preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK",style: UIAlertActionStyle.default)
            
        {
            action in
            self.dismiss(animated: true, completion: nil)
        }
        myAlert.addAction(okAction)
        self.present(myAlert,animated: true, completion: nil)
        
        
    }
    func displayMyAlertMessage(_usermessage : String)
    {
        var myalert = UIAlertController(title: "Alert",message: _usermessage,preferredStyle: UIAlertControllerStyle.alert)
        let okaction = UIAlertAction(title: "OK", style:UIAlertActionStyle.default,handler:nil)
        myalert.addAction(okaction)
        self.present(myalert,animated: true,completion: nil)
        
        
    }

    @IBAction func othrbtn(_ sender: UIButton) {
        
       // performSegue(withIdentifier: "other", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtnote.resignFirstResponder()
        return true
}

}
