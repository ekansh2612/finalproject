//
//  appViewController.swift
//  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit

class appViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var namePicker: UIPickerView!
    
    @IBOutlet weak var citypicker: UIPickerView!
    
    @IBOutlet weak var datepicker: UITextField!
    
    
    var name: [String] = []
    var city: [String] = []
    let datePicker = UIDatePicker()
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        namePicker.dataSource = self
        namePicker.delegate = self
        citypicker.delegate = self
        citypicker.dataSource = self
        
        createDatePicker()
        
    }
    
    //toolbar
    
    func createDatePicker()
    {
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            //bar button
            
        let donebutton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        toolbar.setItems([donebutton], animated: false)
            
            datepicker.inputAccessoryView = toolbar
            //assigning datepicker to text field
            
            datepicker.inputView = datePicker
        
        
        
        }
    @objc func donePressed()
    {
        datepicker.text = "\(datePicker.date)"
        self.view.endEditing(true)
    }
    
        // Do any additional setup after loading the view.
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1{
            return nameAllClients.count
        }else {
            return nameallcities.count
        }
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1{
            return nameAllClients[row]
        }else {
            return nameallcities[row]
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let desvcc = segue.destination as! mapViewController
   // let namev = name[namePicker.selectedRow(inComponent: 0)]
        
     //   let cityv = city[citypicker.selectedRow(inComponent: 0)]
       
        
        let namev = nameAllClients[namePicker.selectedRow(inComponent: 0)]
        let cityv = nameallcities[citypicker.selectedRow(inComponent: 0)]
        
        desvcc.namex = namev
        desvcc.cityx = cityv
        
        /*let n1 = namePicker.selectedRow(inComponent:0)
        if n1 == 1
        {
            let namev = name[namePicker.selectedRow(inComponent: 0)]
            
            
            desvcc.namex = namev
            
        }
        
        let n2 = citypicker.selectedRow(inComponent:0)
        if n2 == 1
        {
            let cityv = city[citypicker.selectedRow(inComponent: 0)]
         
            desvcc.cityx = cityv
        }*/
    }
    
    
    //@IBOutlet weak var savebutton: UIButton!
    
    
    @IBAction func savebuton(_ sender: UIButton)
    {
        
        performSegue(withIdentifier: "map", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
