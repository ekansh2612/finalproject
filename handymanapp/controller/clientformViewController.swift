//
//  clientformViewController.swift
//  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit

class clientformViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var lblem: UILabel!
    @IBOutlet weak var txtem: UITextField!
    @IBOutlet weak var lbladd: UILabel!
    @IBOutlet weak var txtadd: UITextField!
    @IBOutlet weak var lblocc: UILabel!
    @IBOutlet weak var txtocc: UITextField!
    @IBOutlet weak var lblmob: UILabel!
    @IBOutlet weak var txtmob: UITextField!
    @IBOutlet weak var lbljob: UILabel!
    @IBOutlet weak var txtcase: UITextField!
    @IBOutlet weak var lblcity: UILabel!
    @IBOutlet weak var txtcity: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtem.delegate = self
        txtadd.delegate = self
        txtmob.delegate = self
        txtocc.delegate = self
        txtcity.delegate = self
        txtcase.delegate = self
        txtname.delegate = self
     
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       txtname.resignFirstResponder()
       txtcase.resignFirstResponder()
       txtcity.resignFirstResponder()
       txtocc.resignFirstResponder()
       txtadd.resignFirstResponder()
        txtem.resignFirstResponder()
       
        return true
    }
  
    
    @IBAction func canbtn(_ sender: UIButton)
    {
        performSegue(withIdentifier: "cancel", sender: self)    }
    
    @IBAction func savebtn(_: UIButton)
    {
        if txtname.text != "" && txtcase.text != "" && txtcity.text != "" && txtocc.text != "" && txtadd.text != "" && txtem.text != ""
           {
    performSegue(withIdentifier: "confirm", sender: self)
            
            
        }
        else
        {
            var myAlert = UIAlertController(title:"Alert",message:"Enter all the fields",preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK",style: UIAlertActionStyle.default)
            
            {
                action in
                self.dismiss(animated: true, completion: nil)
            }
            myAlert.addAction(okAction)
            self.present(myAlert,animated: true, completion: nil)
            
        }
        
  func displayMyAlertMessage(_usermessage : String)
        {
            var myalert = UIAlertController(title: "Alert",message: _usermessage,preferredStyle: UIAlertControllerStyle.alert)
            let okaction = UIAlertAction(title: "OK", style:UIAlertActionStyle.default,handler:nil)
            myalert.addAction(okaction)
            self.present(myalert,animated: true,completion: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! confirmsaveViewController
        
        vc.nm = txtname.text!
        vc.ad = txtadd.text!
        vc.ct = txtcity.text!
        vc.cs = txtcase.text!
        vc.oc = txtocc.text!
        vc.em = txtem.text!
        vc.mb = txtmob.text!
      
    }
    
}



