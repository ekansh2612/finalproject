//
//  clientViewController.swift
//  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit

class clientViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    

    var name = ["Ashish","Varun Singh","Manbir Kaur","Kiran Ghotra","Gurjinder Singh","Vikas Rana","Rahul Raj","Rashpal Singh","Raj Kmar"]
    
    var emailId = ["ashish234@gmail.com","singhv43@yahoo.com","sohnikudi32@gmail.com","Kiran289@ymail.com","ggn99@gmail.com","rana8696@gmail.com","singhpunjabi5@gmail.com","raj87@ymail.com","Rashu543@gmail.com","sohnamunda564@gmail.com"]
    
    var address = ["12 Front St W, Brampton","50 Queen St W, Toronto"," 145 Richmond St W, Brampton"," 55 Hallcrown Pl, North York"," 90 Yonge St, Vaughan"," 260 Queen St E, Brampton","90 Biscayne Crescent, Brampton","1330 Fewster Drive, Mississauga"," 10 Nevets Road, Brampton","150 Westcreek Blvd, Kitchener"]
    
    var occupation = ["International Student","Software Engineer","Teacher","Graduate Student","Factory worker","fork lifter"," Tim's Manager","Dentist","Doctor","Free lancer worker"]
    
    var mobile_no = ["647-327-9827","447-547-9675","536-6884-5685","645-546-4647","536-421-4575","648-356-3567","782-353-2464","537-341-5463","673-245-4231","745-351-1341"]
    
    var jobdes = ["basement reno","furniture repair","paint","plumbing","bathroom repair","potlights fitting","floor tiles","grass cutting","house painting","furniture repair"]
    
    var city = ["Brampton","Toronto","Brampton","Toronto","Vaughan","Brampton","Brampton","Mississauga","Brampton","Kitchener"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return name.count
            }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 320
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        let cl = tableView.dequeueReusableCell(withIdentifier: "clientTableViewCell", for: indexPath) as? clientTableViewCell
        
        cl?.lblna.text = "\(name[indexPath.row])"
        cl?.lblem.text = "\(emailId[indexPath.row])"
        cl?.lblad.text = "\(address[indexPath.row])"
       cl?.lbloc.text = "\(occupation [indexPath.row])"
       cl?.lblmo.text = "\(mobile_no[indexPath.row])"
        cl?.lblca.text = "\(jobdes[indexPath.row])"
        cl?.lblci.text = "\(city[indexPath.row])"
        
        return cl!
        
        }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
  //  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
   // }

}

