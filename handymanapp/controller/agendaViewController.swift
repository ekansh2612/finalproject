//
//  agendaViewController.swift
///  handymanapp
//
//  Created by MacStudent on 2019-08-15.
//  Copyright © 2019 Ekansh sharma. All rights reserved.
//

import UIKit
import  MapKit

final class map1Annotation: NSObject ,MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?)
    {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
}

class agendaViewController: UIViewController

{
    
    @IBOutlet weak var mapView2: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var x1 : Int = 0
        
        for a in nameallcities
        {
        
        mapView2.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        
        let longitudec : Double = Double(long[x1])!
        let latitudec : Double = Double(lat[x1])!
            let cityx = a
        let namex = ""
            
        let citycoordinates = CLLocationCoordinate2D(latitude: latitudec, longitude: longitudec)
        
        let cityannotation = map1Annotation(coordinate: citycoordinates, title: cityx, subtitle: namex)
        
        mapView2.addAnnotation(cityannotation)
        
            x1 = x1 + 1;
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension  agendaViewController  : MKMapViewDelegate
{
    func mapView2(_ mapView2 :MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if let ann = mapView2.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier) as? MKMarkerAnnotationView
        {
            ann.animatesWhenAdded = true
            ann.titleVisibility = .adaptive
            ann.titleVisibility = .adaptive
            
            return ann
            
        }
        return nil
        
        
    }
}
