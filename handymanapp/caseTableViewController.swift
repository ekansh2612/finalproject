//
//  caseTableViewController.swift
//  lawyercasediary
//
//  Created by MacStudent on 2018-03-24.
//  Copyright © 2018 kamla sharma. All rights reserved.
//

import UIKit

class caseTableViewController: UITableViewController, UITableViewDelegate
{
    var name1 = ["Ashish","Varun Singh","Rajvir Hudda","Kiran Ghotra","Gurjinder Singh","Vikas Rana","Rahul Raj","Rashpal Singh","Raj Kmar"]
    
    var emailId1 = ["ashish234@gmail.com","singhv43@yahoo.com","rjhudda2@gmail.com","Kiran289@ymail.com","ggn99@gmail.com","rana8696@gmail.com","singhpunjabi5@gmail.com","raj87@ymail.com"]
    
    var address1 = ["12 Front St W, Brampton","50 Queen St W, Toronto"," 145 Richmond St W, Brampton"," 55 Hallcrown Pl, North York"," 90 Yonge St, Vaughan"," 260 Queen St E, Brampton","90 Biscayne Crescent, Brampton","1330 Fewster Drive, Mississauga"," 10 Nevets Road, Brampton","150 Westcreek Blvd, Kitchener"]
    
    var occupation1 = ["International Student","Software Engineer","Teacher","Graduate Student","Factory worker","fork lifter"," Tim's Manager","Dentist","Doctor","Free lancer worker"]
    
    var mobile_no1 = ["647-327-9827","447-547-9675","536-6884-5685","645-546-4647","536-421-4575","648-356-3567","782-353-2464","537-341-5463","673-245-4231","745-351-1341"]
    
    var casedes1 = ["Landlord Issue","Tort Claims","Road Accident","murder case claim","Road Accident","Chain snatching","Dowry Case","Petition case","Divorce case","Property Dispute"]
    
    var city1 = ["Brampton","Toronto","Brampton","Toronto","Vaughan","Brampton","Brampton","Mississauga","Brampton","Kitchener"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve
       // selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return name.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
 

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    
    /*func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cl2 = tableView.dequeueReusableCell(withIdentifier: "clientableViewCell", for: indexPath) as? clientTableViewCell
        cl2?.lblna.text = "\(name1[indexPath.row])"
         //cl2?.lblem.text = "\(name1[indexPath.row])"
      //  cl2?.lblad.text = "\(name[indexPath.row])"
      //  cl2?.lbloc.text = "\(name[indexPath.row])"
       // cl2?.lblmo.text = "\(name[indexPath.row])"
       // cl2?.lblca.text = "\(name[indexPath.row])"
       // cl2?.lblci.text = "\(name[indexPath.row])"
        return cl2!
    }*/
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
